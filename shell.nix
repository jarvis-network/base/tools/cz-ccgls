{ pkgs ? import <nixpkgs> { } }: with pkgs;
mkShell {
  buildInputs = [
    fish
    git
    jq
    nodejs-14_x
    (yarn.override { nodejs = nodejs-14_x; })
  ];

  shellHook = ''
    echo "Welcome to Jarvis Nix Shell"
    export NODE_PATH=$PWD/.nix-node
    export NPM_CONFIG_PREFIX=$PWD/.nix-node
    export PATH=$NODE_PATH/bin:$PATH
  '';
}
